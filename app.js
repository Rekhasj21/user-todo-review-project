const url = 'https://jsonplaceholder.typicode.com/users';
const todosElement = document.getElementById('todoList')

fetch(url)
    .then(response => response.json())
    .then(users => {
        const selectedUsers = users.slice(0, 5);

        const todoPromises = selectedUsers.map(user => {
            return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}`)
                .then(response => response.json())
                .then(todos => {
                    const userTodos = {
                        name: user.name,
                        todos: todos
                    };
                    return userTodos;
                });
        })
        Promise.all(todoPromises)
            .then(userTodosList => {
                const todoList = document.getElementById('todo-list');
                userTodosList.forEach(userTodos => {
                    const userTodoElement = createUserTodoElement(userTodos);
                })
                    .catch(error => console.error(error));
            })
    })

function createUserTodoElement(userTodos) {
    console.log(userTodos)
    const userTodoElement = document.createElement('li');
    todosElement.appendChild(userTodoElement)

    const userNameElement = document.createElement('h1');
    userNameElement.textContent = userTodos.name;

    userTodoElement.appendChild(userNameElement);

    const todoListElement = document.createElement('ul');
    userTodos.todos.forEach(todo => {
        const todoElement = document.createElement('li');
        todoElement.textContent = `Id: ${todo.id}  Title: ${todo.title} Status: ${todo.completed}`;
        todoListElement.appendChild(todoElement);
    });
    userTodoElement.appendChild(todoListElement);

    return userTodoElement;
}
